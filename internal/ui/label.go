package ui

import (
	"fmt"

	tb "github.com/nsf/termbox-go"
)

type Label struct {
	View
	text      string
	alignment Alignment
}

type LabelOption func(*Label)

func WithText(format string, args ...interface{}) LabelOption {
	return func(l *Label) {
		l.SetText(format, args...)
	}
}

func WithAlignment(alignment Alignment) LabelOption {
	return func(l *Label) {
		l.alignment = alignment
	}
}

func NewLabel(options ...interface{}) *Label {
	l := &Label{
		View: *NewView(
			WithStyle(tb.ColorDefault, tb.ColorDefault),
		),
	}

	for _, option := range options {
		switch opt := option.(type) {
		case ViewOption:
			opt(&l.View)
		case LabelOption:
			opt(l)
		}
	}

	return l
}

func (l *Label) SetText(format string, args ...interface{}) {
	l.text = fmt.Sprintf(format, args...)
}

func (l *Label) Render(p *Geometry) {
	if l.text == "" {
		return
	}

	l.View.Render(p)

	v := p.Clamp(&l.Geometry)
	width := v.right - v.left

	pad := 0

	if l.alignment == AlignCenter {
		pad = (width / 2) - (len(l.text) / 2)
	}

	if l.alignment == AlignRight {
		pad = width - len(l.text)
	}

	for idx, ch := range l.text {
		x := v.left + pad + idx
		y := v.top

		if x < v.left || x >= v.right {
			continue
		}

		cell := GetCell(x, y)
		tb.SetCell(x, y, ch, cell.Fg, cell.Bg)
	}
}
