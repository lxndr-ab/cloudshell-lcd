package ui

type Gauge struct {
	View
	bar     *View
	label   *Label
	percent float32
}

func NewGauge(options ...interface{}) *Gauge {
	g := &Gauge{
		View: *NewView(
			WithSize(Infinite, 1),
			WithStyle(GaugeFg, GaugeBg),
		),
		bar: NewView(
			WithSize(0, Infinite),
			WithStyle(GaugeBarFg, GaugeBarBg),
		),
		label: NewLabel(
			WithAlignment(AlignCenter),
		),
	}

	g.children = append(g.children, g.bar, g.label)

	for _, option := range options {
		switch opt := option.(type) {
		case ViewOption:
			opt(&g.View)
		}
	}

	return g
}

func (g *Gauge) SetPercent(percent float32) {
	g.percent = percent
}

func (g *Gauge) SetLabel(format string, args ...interface{}) {
	g.label.SetText(format, args...)
}

func (g *Gauge) Render(p *Geometry) {
	v := p.Clamp(&g.Geometry)
	width := v.right - v.left
	prog := int(float32(width) / 100 * g.percent)
	g.bar.right = prog

	g.View.Render(p)
}
