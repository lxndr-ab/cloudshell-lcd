package ui

import tb "github.com/nsf/termbox-go"

const (
	ViewFg = tb.ColorWhite
	ViewBg = tb.ColorBlack

	GaugeFg    = tb.ColorBlack
	GaugeBg    = tb.ColorWhite
	GaugeBarFg = tb.ColorWhite
	GaugeBarBg = tb.ColorBlue
)
