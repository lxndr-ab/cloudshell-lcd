package ui

type Geometry struct {
	left, top     int
	right, bottom int
}

func (g *Geometry) Clamp(in *Geometry) *Geometry {
	return &Geometry{
		left:   Max(g.left, g.left+in.left),
		top:    Max(g.top, g.top+in.top),
		right:  Min(g.right, g.left+in.right),
		bottom: Min(g.bottom, g.top+in.bottom),
	}
}
