package ui

import tb "github.com/nsf/termbox-go"

type View struct {
	Geometry
	fg, bg   tb.Attribute
	children []Renderable
}

type ViewOption func(*View)

func WithPos(x, y int) ViewOption {
	return func(l *View) {
		l.left = x
		l.top = y
	}
}

func WithSize(w, h int) ViewOption {
	return func(l *View) {
		l.right = l.left + w
		l.bottom = l.top + h
	}
}

func WithStyle(fg, bg tb.Attribute) ViewOption {
	return func(l *View) {
		l.fg = fg
		l.bg = bg
	}
}

func NewView(options ...interface{}) *View {
	v := &View{
		Geometry: Geometry{
			left:   0,
			top:    0,
			right:  Infinite,
			bottom: Infinite,
		},
		bg: ViewBg,
		fg: ViewFg,
	}

	for _, option := range options {
		switch opt := option.(type) {
		case ViewOption:
			opt(v)
		case Renderable:
			v.children = append(v.children, opt)
		}
	}

	return v
}

func (v *View) Render(p *Geometry) {
	v.RenderBackground(p)
	v.RenderChildren(p)
}

func (w *View) RenderBackground(p *Geometry) {
	v := p.Clamp(&w.Geometry)

	for y := v.top; y < v.bottom; y++ {
		for x := v.left; x < v.right; x++ {
			cell := GetCell(x, y)
			fg := cell.Fg
			bg := cell.Bg

			if w.fg > tb.ColorDefault {
				fg = w.fg
			}

			if w.bg > tb.ColorDefault {
				bg = w.bg
			}

			tb.SetCell(x, y, ' ', fg, bg)
		}
	}
}

func (w *View) RenderChildren(p *Geometry) {
	v := p.Clamp(&w.Geometry)

	for _, rnd := range w.children {
		rnd.Render(v)
	}
}
