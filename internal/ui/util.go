package ui

import (
	"math"

	tb "github.com/nsf/termbox-go"
)

const Infinite = int(math.MaxInt16)

type Alignment uint

const (
	AlignLeft Alignment = iota
	AlignCenter
	AlignRight
)

func Max(a, b int) int {
	if a > b {
		return a
	}

	return b
}

func Min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func GetCell(x, y int) tb.Cell {
	w, h := tb.Size()

	if x < 0 || x >= w {
		panic("x is incorrect")
	}

	if y < 0 || y >= h {
		panic("y is incorrect")
	}

	buf := tb.CellBuffer()
	return buf[y*w+x]
}
