package ui

type Renderable interface {
	Render(g *Geometry)
}
