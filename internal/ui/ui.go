package ui

import tb "github.com/nsf/termbox-go"

type UI struct {
	rt Renderable
}

func NewUI(rt Renderable) *UI {
	err := tb.Init()

	if err != nil {
		panic(err)
	}

	ui := &UI{
		rt: rt,
	}

	return ui
}

func (ui *UI) Close() {
	tb.Close()
}

func (ui *UI) Render() {
	w, h := tb.Size()

	g := &Geometry{
		right:  w,
		bottom: h,
	}

	ui.rt.Render(g)
	tb.Flush()
}

func (ui *UI) Run() chan tb.Event {
	ui.Render()

	ch := make(chan tb.Event)

	go func() {
		for {
			ch <- tb.PollEvent()
		}
	}()

	return ch
}
