package app

import "os"

type Config struct {
	diskName string
	netIface string
	trAddr   string
}

func GetEnv(env, def string) string {
	v := os.Getenv(env)

	if v == "" {
		return def
	}

	return v
}

func NewConfig() *Config {
	return &Config{
		diskName: GetEnv("CS_DISK_NAME", "/dev/sda"),
		netIface: GetEnv("CS_NET_IFACE", "eth0"),
		trAddr:   GetEnv("CS_TR_ADDR", "http://localhost:9091/transmission/rpc"),
	}
}
