package app

import (
	"time"

	tb "github.com/nsf/termbox-go"

	"cloudshell-lcd/internal/inf"
	"cloudshell-lcd/internal/ui"
)

type App struct {
	config    *Config
	ui        *ui.UI
	uptime    *ui.Label
	nprocs    *ui.Label
	cpuGauge  *ui.Gauge
	cpuTemp   *ui.Label
	memGauge  *ui.Gauge
	hddGauge  *ui.Gauge
	hddTemp   *ui.Label
	dck       *ui.Label
	ip        *ui.Label
	trnTCount *ui.Label
	trnFCount *ui.Label
	trnDCount *ui.Label
	trnPCount *ui.Label
	trnECount *ui.Label
	errBox    *ui.Label
}

func NewApp() *App {
	a := &App{
		config: NewConfig(),
	}

	uptimeTitle := ui.NewLabel(ui.WithText("UPT"))
	a.uptime = ui.NewLabel(ui.WithPos(4, 0))

	nprocsTitle := ui.NewLabel(ui.WithPos(32, 0), ui.WithText("PRC"))
	a.nprocs = ui.NewLabel(ui.WithPos(36, 0))

	cpuTitle := ui.NewLabel(ui.WithText("CPU"))
	a.cpuGauge = ui.NewGauge(ui.WithPos(4, 0), ui.WithSize(32, 1))
	a.cpuTemp = ui.NewLabel(ui.WithPos(37, 0), ui.WithStyle(tb.ColorGreen, tb.ColorDefault))

	memTitle := ui.NewLabel(ui.WithText("MEM"))
	a.memGauge = ui.NewGauge(ui.WithPos(4, 0), ui.WithSize(32, 1))

	hddTitle := ui.NewLabel(ui.WithText("HDD"))
	a.hddGauge = ui.NewGauge(ui.WithPos(4, 0), ui.WithSize(32, 1))
	a.hddTemp = ui.NewLabel(ui.WithPos(37, 0), ui.WithStyle(tb.ColorGreen, tb.ColorDefault))

	dckTitle := ui.NewLabel(ui.WithText("DCK"))
	a.dck = ui.NewLabel(ui.WithPos(4, 0))

	netTitle := ui.NewLabel(ui.WithText("NET"))
	a.ip = ui.NewLabel(ui.WithPos(4, 0))

	trnTitle := ui.NewLabel(ui.WithText("TRN"))
	a.trnTCount = ui.NewLabel(
		ui.WithPos(4, 0),
		ui.WithStyle(tb.ColorWhite, tb.ColorDefault),
	)
	a.trnFCount = ui.NewLabel(
		ui.WithPos(8, 0),
		ui.WithStyle(tb.ColorGreen, tb.ColorDefault),
	)
	a.trnDCount = ui.NewLabel(
		ui.WithPos(12, 0),
		ui.WithStyle(tb.ColorYellow, tb.ColorDefault),
	)
	a.trnPCount = ui.NewLabel(
		ui.WithPos(16, 0),
		ui.WithStyle(tb.ColorBlue, tb.ColorDefault),
	)
	a.trnECount = ui.NewLabel(
		ui.WithPos(20, 0),
		ui.WithStyle(tb.ColorRed, tb.ColorDefault),
	)

	a.errBox = ui.NewLabel(
		ui.WithStyle(tb.ColorWhite, tb.ColorRed),
	)

	a.ui = ui.NewUI(
		ui.NewView(
			ui.NewView(ui.WithPos(0, 0), uptimeTitle, a.uptime, nprocsTitle, a.nprocs),
			ui.NewView(ui.WithPos(0, 1), cpuTitle, a.cpuGauge, a.cpuTemp),
			ui.NewView(ui.WithPos(0, 2), memTitle, a.memGauge),
			ui.NewView(ui.WithPos(0, 3), hddTitle, a.hddGauge, a.hddTemp),
			ui.NewView(ui.WithPos(0, 5), dckTitle, a.dck),
			ui.NewView(ui.WithPos(0, 6), netTitle, a.ip),
			ui.NewView(ui.WithPos(0, 8), trnTitle, a.trnTCount, a.trnFCount, a.trnDCount, a.trnPCount, a.trnECount),
			ui.NewView(ui.WithPos(0, 10), a.errBox),
		),
	)

	err := inf.OpenDocker()

	if err != nil {
		a.errBox.SetText(err.Error())
	}

	return a
}

func (a *App) Run() {
	sTicker := time.NewTicker(time.Second)
	s5Ticker := time.NewTicker(time.Second)
	mTicker := time.NewTicker(time.Minute)
	hTicker := time.NewTicker(time.Hour)

	a.UpdateUptime()
	a.UpdateProcs()
	a.UpdateCPU()
	a.UpdateMemory()
	a.UpdateDisk()
	a.UpdateDocker()
	a.UpdateNet()
	a.UpdateTransmission()

	for {
		select {
		case <-sTicker.C:
			a.UpdateUptime()
		case <-s5Ticker.C:
			a.UpdateProcs()
			a.UpdateCPU()
			a.UpdateMemory()
		case <-mTicker.C:
			a.UpdateDisk()
			a.UpdateDocker()
			a.UpdateTransmission()
		case <-hTicker.C:
			a.UpdateNet()
		}

		a.ui.Render()
	}
}

func (a *App) Close() {
	a.ui.Close()
}

func (a *App) UpdateUptime() {
	secs := inf.Uptime()

	a.uptime.SetText(
		"%3dd %2dh %2dm %2ds",
		secs/60/60/24,
		secs/60/60%24,
		secs/60%60,
		secs%60,
	)
}

func (a *App) UpdateProcs() {
	a.nprocs.SetText("%4d", inf.NumProcesses())
}

func (a *App) UpdateCPU() {
	l1, l5, l15, lp := inf.CPULoad()
	a.cpuGauge.SetPercent(lp)
	a.cpuGauge.SetLabel("%.02f / %.02f / %.02f", l1, l5, l15)
	a.cpuTemp.SetText("%2.f°", inf.CPUTemp())
}

func (a *App) UpdateMemory() {
	t, u, p := inf.GetMemoryUsage()

	a.memGauge.SetPercent(p)
	a.memGauge.SetLabel(
		"%.02f / %.02f MB",
		float32(u)/1024,
		float32(t)/1024,
	)
}

func (a *App) UpdateDisk() {
	ht, hu, hp := inf.GetDiskUsage()

	a.hddGauge.SetPercent(hp)
	a.hddGauge.SetLabel(
		"%.02f / %.02f GB",
		float64(hu)/1024/1024/1024,
		float64(ht)/1024/1024/1024,
	)

	temp, err := inf.GetDiskTemperature(a.config.diskName)

	if err != nil {
		a.errBox.SetText(err.Error())
	}

	a.hddTemp.SetText("%2.f°", temp)
}

func (a *App) UpdateDocker() {
	c, s, t := inf.GetDockerInfo()

	a.dck.SetText("C%d S%d T%d", c, s, t)
}

func (a *App) UpdateNet() {
	loc, err := inf.GetLocalIP(a.config.netIface)

	if err != nil {
		a.errBox.SetText(err.Error())
	}

	ext, err := inf.GetExternalIP()

	if err != nil {
		a.errBox.SetText(err.Error())
	}

	a.ip.SetText("%s ext %s", loc, ext)
}

func (a *App) UpdateTransmission() {
	torrents, err := inf.GetTorrents(a.config.trAddr)

	if err != nil {
		a.errBox.SetText(err.Error())
	}

	var cTorrents = len(torrents)
	var cPaused = 0
	var cDownloading = 0
	var cFinished = 0
	var cError = 0

	for _, t := range torrents {
		if t.Error != 0 {
			cError++
			continue
		}

		if t.PercentDone >= 1 {
			cFinished++
		}

		switch t.Status {
		case 0: // paused
			cPaused++
		case 4: // downloading
			cDownloading++
		}
	}

	a.trnTCount.SetText("%3d", cTorrents)
	a.trnFCount.SetText("%3d", cFinished)
	a.trnDCount.SetText("%3d", cDownloading)
	a.trnPCount.SetText("%3d", cPaused)
	a.trnECount.SetText("%3d", cError)
}
