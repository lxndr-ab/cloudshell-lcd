package inf

import "syscall"

func GetDiskUsage() (uint64, uint64, float32) {
	var stat syscall.Statfs_t
	syscall.Statfs("/home", &stat)

	blockSize := uint64(stat.Bsize)
	blocksTotal := uint64(stat.Blocks)
	blocksAvail := uint64(stat.Bavail)
	blocksUsed := blocksTotal - blocksAvail

	total := blocksTotal * blockSize
	used := blocksUsed * blockSize
	perc := blocksUsed / (blocksTotal / 100)

	return total, used, float32(perc)
}
