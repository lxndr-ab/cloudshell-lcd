package inf

import (
	"runtime"
	"syscall"
)

const scale = 65536

var cpus int = runtime.NumCPU()

func CPULoad() (float32, float32, float32, float32) {
	var si syscall.Sysinfo_t
	err := syscall.Sysinfo(&si)

	if err != nil {
		panic(err)
	}

	l1 := float32(si.Loads[0])
	l5 := float32(si.Loads[1])
	l15 := float32(si.Loads[2])

	return l1 / scale, l5 / scale, l15 / scale,
		l1 / (scale * float32(cpus) / 100)
}
