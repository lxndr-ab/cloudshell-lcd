package inf

import (
	"io/ioutil"
	"strconv"
	"strings"
)

const cpuTempPath = "/sys/class/thermal/thermal_zone0/temp"

func CPUTemp() float32 {
	content, err := ioutil.ReadFile(cpuTempPath)

	if err != nil {
		panic(err)
	}

	text := strings.TrimSpace(string(content))
	temp, err := strconv.ParseFloat(text, 32)

	if err != nil {
		panic(err)
	}

	return float32(temp) / 1000
}
