package inf

import (
	"fmt"
	"os/exec"
	"regexp"
	"strconv"
)

var re = regexp.MustCompile(`(?m)^194 Temperature_Celsius\s+\w+\s+\w+\s+\w+\s+\w+\s+\w+\s+\w+\s+-\s+(\d+)`)
var reErr = regexp.MustCompile(`(?m)^Smartctl (.+)$`)

func GetDiskTemperature(diskName string) (float32, error) {
	cmd := "smartctl"
	args := []string{"-A", diskName}

	out, err := exec.Command(cmd, args...).Output()

	if err != nil {
		m := reErr.FindAllStringSubmatch(string(out), -1)

		if len(m) == 0 {
			return 0, fmt.Errorf("smartctl: %v", err)
		}

		return 0, fmt.Errorf("smartctl: %s", m[0][1])
	}

	m := re.FindAllStringSubmatch(string(out), -1)

	if len(m) == 0 {
		return 0, fmt.Errorf("disk-temp: could not parse smartctl output")
	}

	temp, err := strconv.ParseFloat(m[0][1], 32)

	if err != nil {
		panic(err)
	}

	return float32(temp), nil
}
