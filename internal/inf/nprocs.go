package inf

import "syscall"

func NumProcesses() uint16 {
	var si syscall.Sysinfo_t
	err := syscall.Sysinfo(&si)

	if err != nil {
		panic(err)
	}

	return si.Procs
}
