package inf

import (
	"io/ioutil"
	"regexp"
	"strconv"
)

var reMem = regexp.MustCompile(`(?m)^(MemTotal|MemAvailable):\s+(\d+) kB$`)

// GetMemoryUsage gets memory usage from /proc/meminfo (in kB)
func GetMemoryUsage() (uint64, uint64, float32) {
	content, err := ioutil.ReadFile("/proc/meminfo")

	if err != nil {
		panic(err)
	}

	var total, avail, used uint64

	for _, match := range reMem.FindAllStringSubmatch(string(content), -1) {
		switch match[1] {
		case "MemTotal":
			val, err := strconv.ParseUint(match[2], 10, 0)

			if err != nil {
				panic(err)
			}

			total = val
		case "MemAvailable":
			val, err := strconv.ParseUint(match[2], 10, 0)

			if err != nil {
				panic(err)
			}

			avail = val
		}
	}

	used = total - avail
	perc := used / (total / 100)

	return total, used, float32(perc)
}
