package inf

import (
	"fmt"
	"net"
)

func GetLocalIP(netIface string) (string, error) {
	ifaces, err := net.Interfaces()

	if err != nil {
		return unknown, fmt.Errorf("local-ip: %v", err)
	}

	for _, i := range ifaces {
		if i.Name != netIface {
			continue
		}

		addrs, err := i.Addrs()

		if err != nil {
			return unknown, fmt.Errorf("local-ip: %v", err)
		}

		for _, addr := range addrs {
			ip, ok := addr.(*net.IPNet)

			if ok {
				return ip.IP.String(), nil
			}
		}
	}

	return unknown, fmt.Errorf("local-ip: unknown interface %s", netIface)
}
