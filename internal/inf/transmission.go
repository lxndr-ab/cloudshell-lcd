package inf

import (
	"github.com/odwrtw/transmission"
)

var trClient *transmission.Client

func GetTorrents(addr string) ([]*transmission.Torrent, error) {
	if trClient == nil {
		conf := transmission.Config{
			Address: addr,
		}

		t, err := transmission.New(conf)

		if err != nil {
			return nil, err
		}

		trClient = t
	}

	torrents, err := trClient.GetTorrents()
	return torrents, err
}
