package inf

import (
	"context"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

var cli *client.Client

func OpenDocker() error {
	c, err := client.NewEnvClient()

	if err != nil {
		return err
	}

	cli = c
	return nil
}

func GetDockerInfo() (int, int, int) {
	cnt, _ := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	srv, _ := cli.ServiceList(context.Background(), types.ServiceListOptions{})
	tsk, _ := cli.ServiceList(context.Background(), types.ServiceListOptions{})
	return len(cnt), len(srv), len(tsk)
}

func CloseDocker() {
	cli.Close()
}
