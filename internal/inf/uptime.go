package inf

import "syscall"

func Uptime() uint32 {
	var si syscall.Sysinfo_t
	err := syscall.Sysinfo(&si)

	if err != nil {
		panic(err)
	}

	return uint32(si.Uptime)
}
