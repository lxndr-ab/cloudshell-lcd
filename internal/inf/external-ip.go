package inf

import (
	"io/ioutil"
	"log"
	"net/http"
)

const url = "https://mdrjr.net/ip.php"

func GetExternalIP() (string, error) {
	resp, err := http.Get(url)

	if err != nil {
		log.Printf("Error getting external IP: GET %s: %v", url, err)
		return unknown, nil
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Printf("Error getting external IP: parsing the responce: %v", err)
		return unknown, nil
	}

	return string(body), nil
}
