module cloudshell-lcd

go 1.13

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/nsf/termbox-go v0.0.0-20200204031403-4d2b513ad8be
	github.com/odwrtw/transmission v0.0.0-20191103153330-cbf08fea0b8c
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/exp v0.0.0-20200228211341-fcea875c7e85 // indirect
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
)
