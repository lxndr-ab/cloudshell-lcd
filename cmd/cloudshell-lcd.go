package main

import "cloudshell-lcd/internal/app"

func main() {
	a := app.NewApp()
	a.Run()
	a.Close()
}
