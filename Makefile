OUTPUT=cloudshell-lcd

all: ${OUTPUT}

${OUTPUT}:
	go build -ldflags="-s -w" cmd/cloudshell-lcd.go

arm7:
	GOARCH=arm GOARM=7 $(MAKE)
